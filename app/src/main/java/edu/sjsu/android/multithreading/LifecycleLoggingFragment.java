package edu.sjsu.android.multithreading;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

public abstract class LifecycleLoggingFragment extends Fragment {
    static private final String TAG = "Lifecycle Logging Fragment";

    @SuppressLint("LongLogTag")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach: fragment attached "+activity.toString());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: fragment detach");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: fragment rebuilt");
        if (savedInstanceState == null) {
            Log.d(TAG, "onCreate: fragment created fresh");
        } else {
            Log.d(TAG, "onCreate: fragment restarted");
        }
    }

    @SuppressLint("LongLogTag")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState, boolean isDynamic) {
        Log.d(TAG, "onCreateView: fragment rebuilt");
        if (savedInstanceState == null) {
            Log.d(TAG, "onCreateView: fragment created fresh");
        } else {
            Log.d(TAG, "onCreateView: fragment restarted");
        }
        if (!(isDynamic)) {
            super.onCreateView(inflater, container, savedInstanceState);
        }
        return null;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView: fragment view destroyed");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @SuppressLint("LongLogTag")
    public void onViewCreated(Bundle savedInstanceState) {
        super.onViewCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: fragment activity created ");
    }

    @SuppressLint("LongLogTag")
    ActivityResultLauncher<Intent> someActivityForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent intent = new Intent(this, ThreadedDownloadActivity.class);
                        ActivityResultLauncher.launch(intent);
                    }
                }
            });

    public void openSomeActivityForResults(int requestCode, int resultCode, Intent data) {
        super.startActivity(requestCode, resultCode, data);
        Log.d(TAG, new StringBuilder("onActivityResult ").
                append(" request=").append(requestCode).
                append(" result=").append(resultCode).
                append(" intent=[").append(data).append("]").
                toString());
    }
}

