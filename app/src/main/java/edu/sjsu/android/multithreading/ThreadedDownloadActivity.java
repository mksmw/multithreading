package edu.sjsu.android.multithreading;

import static android.widget.Toast.LENGTH_LONG;

import java.net.MalformedURLException;
import java.net.URL;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentManager;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.sjsu.android.multithreading.ThreadedDownloadFragment.OnDownloadFaultHandler;


public class ThreadedDownloadActivity extends LifecycleLoggingActivity
        implements OnDownloadFaultHandler {
    static private final String TAG = "Threaded Download Activity";

    private EditText urlEditText = null;
    private ThreadedDownloadFragment imageFragment = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.threaded_download);

        this.urlEditText = (EditText) urlEditText.findViewById(R.id.edit_image_url);

        final FragmentManager fm = this.getSupportFragmentManager();
        final Fragment fobj = fm.findFragmentById(R.id.fragment_container);
        if (fobj == null) {
            final FragmentTransaction txn = fm.beginTransaction();
            this.imageFragment = new ThreadedDownloadFragment();
            txn.add(R.id.fragment_container, this.imageFragment);
            txn.commit();
        } else {
            this.imageFragment = (ThreadedDownloadFragment) fobj;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @SuppressLint("LongLogTag")
    private URL getValidUrlFromWidget() {
        final Editable urlEditable = this.urlEditText.getText();
        if (urlEditable.length() < 1) {
            final String urlStr = this.urlEditText.getHint().toString();
            try {
                return new URL(urlStr);
            } catch (MalformedURLException e) {
                Log.e(TAG, "hard coded, should never happen");
                return null;
            }
        }
        final String urlStr = urlEditable.toString();
        try {
            return new URL(urlStr);
        } catch (MalformedURLException ex) {
            Log.i(TAG, "malformed url " + urlStr);
        }
        final CharSequence errorMsg = this.getResources().getText(
                R.string.error_malformed_url);
        this.urlEditText.setError(errorMsg);
        Toast.makeText(this, errorMsg, LENGTH_LONG).show();
        return null;
    }

    public void resetImage(View view) {
        if (this.imageFragment == null)
            return;
        this.imageFragment.resetImage(view);
    }

    public void runAsyncTask(View view) {
        if (this.imageFragment == null)
            return;
        final URL url = getValidUrlFromWidget();
        this.imageFragment.runAsyncTask(view, url);
    }

    public void runMessages(View view) {
        if (this.imageFragment == null)
            return;
        final URL url = getValidUrlFromWidget();
        this.imageFragment.runMessages(view, url);
    }

    public void runRunnable(View view) {
        if (this.imageFragment == null)
            return;
        final URL url = getValidUrlFromWidget();
        this.imageFragment.runRunnable(view, url);
    }

    public void onFault(final CharSequence msg) {
        this.runOnUiThread(new Runnable() {
            final CharSequence msg_ = msg;
            final ThreadedDownloadActivity master = ThreadedDownloadActivity.this;

            public void run() {
                Toast.makeText(master, msg, LENGTH_LONG).show();

                final Drawable dr = master.getResources().getDrawable(
                        R.drawable.indicator_input_warn);
                dr.setBounds(0, 0, dr.getIntrinsicWidth(),
                        dr.getIntrinsicHeight());
                master.urlEditText.setError(msg_, dr);
            }
        });
    }
}