package edu.sjsu.android.multithreading;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;

public abstract class LifecycleLoggingActivity extends FragmentActivity {
    static private final String TAG = "Lifecycle Logging Activity";

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: activity rebuilt");
        if (savedInstanceState == null) {
            Log.d(TAG, "onCreate: activity created fresh");
        } else {
            Log.d(TAG, "onCreate: activity restarted");
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState == null) {
            Log.d(TAG, "onRestoreInstanceState: activity created fresh");
        } else {
            Log.d(TAG, "onRestoreInstanceState: activity restarted");
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, new StringBuilder("onActivityResult ").
                append(" request=").append(requestCode).
                append(" result=").append(resultCode).
                append(" intent=[").append(data).append("]").
                toString());
    }
}







